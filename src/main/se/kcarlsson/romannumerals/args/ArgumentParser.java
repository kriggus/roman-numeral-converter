package se.kcarlsson.romannumerals.args;

import static java.lang.Integer.parseInt;

public class ArgumentParser {

    public boolean shouldConvertToRoman(String[] args) {
        return isValidFormat(args) && hasSomeOf(args, "-tr", "--to-roman");
    }

    public boolean shouldConvertFromRoman(String[] args) {
        return isValidFormat(args) && hasSomeOf(args, "-tn", "--to-number");
    }

    public boolean shouldConvertToLowerCase(String[] args) {
        return isValidFormat(args) && shouldConvertToRoman(args) && hasSomeOf(args, "-lc", "--lower-case");
    }

    public String inputAsString(String[] args) {
        if (isInvalidFormat(args)) return "";
        return args[args.length - 1].toUpperCase();
    }

    public int inputAsInt(String[] args) {
        try {
            return parseInt(inputAsString(args));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private boolean isValidFormat(String[] args) {
        return args != null && args.length > 1;
    }

    private boolean isInvalidFormat(String[] args) {
        return !isValidFormat(args);
    }

    private boolean hasSomeOf(String[] ss, String... testFor) {
        for (String s : ss)
            for (String tfs : testFor)
                if (s.toUpperCase().equals(tfs.toUpperCase()))
                    return true;
        return false;
    }
}
