package se.kcarlsson.romannumerals;

import se.kcarlsson.romannumerals.args.ArgumentParser;
import se.kcarlsson.romannumerals.convert.Converter;
import se.kcarlsson.romannumerals.print.Printer;

public class ConversionJob implements Runnable {

    private final Converter converter;
    private final ArgumentParser argParser;
    private final Printer printer;
    private final String[] args;

    ConversionJob(Converter converter, ArgumentParser argParser, Printer printer, String[] args) {
        this.converter = converter;
        this.argParser = argParser;
        this.printer = printer;
        this.args = args;
    }

    @Override
    public void run() {
        if (argParser.shouldConvertToRoman(args))
            convertToRoman(args);
        else if (argParser.shouldConvertFromRoman(args))
            convertFromRoman(args);
        else
            printer.printHelp();
    }

    private void convertToRoman(String[] args) {
        int input = argParser.inputAsInt(args);
        String output = convertToRomanWithLowerCaseSupport(args, input);
        if (output.equals(""))
            printer.printHelp();
        else
            printer.print(output);
    }

    private String convertToRomanWithLowerCaseSupport(String[] args, int input) {
        if (argParser.shouldConvertToLowerCase(args))
            return converter.toRomanNumeralLowerCase(input);
        else
            return converter.toRomanNumeral(input);
    }

    private void convertFromRoman(String[] args) {
        String input = argParser.inputAsString(args);
        int output = converter.fromRomanNumeral(input);
        if (output == 0)
            printer.printHelp();
        else
            printer.print(output);
    }
}
