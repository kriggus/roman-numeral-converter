package se.kcarlsson.romannumerals.convert;

import static se.kcarlsson.romannumerals.convert.RomanSymbolUtils.cdm;
import static se.kcarlsson.romannumerals.convert.RomanSymbolUtils.ivx;
import static se.kcarlsson.romannumerals.convert.RomanSymbolUtils.xlc;

public class Converter {

    public String toRomanNumeralLowerCase(int n) {
        return toRomanNumeral(n).toLowerCase();
    }

    public String toRomanNumeral(int n) {
        var calc1000 = new ToRomanCalculation(n, 3, cdm);
        var calc100 = new ToRomanCalculation(calc1000.remainder, 2, cdm);
        var calc10 = new ToRomanCalculation(calc100.remainder, 1, xlc);
        var calc0 = new ToRomanCalculation(calc10.remainder, 0, ivx);

        return calc1000.symbols + calc100.symbols + calc10.symbols + calc0.symbols;
    }

    public int fromRomanNumeral(String s) {
        var calc0 = new FromRomanCalculation(s, 0, ivx);
        var calc10 = new FromRomanCalculation(calc0.remainder, 1, xlc);
        var calc100 = new FromRomanCalculation(calc10.remainder, 2, cdm);
        var calc1000 = new FromRomanCalculation(calc100.remainder, 3, cdm);

        return calc1000.number + calc100.number + calc10.number + calc0.number;
    }
}
