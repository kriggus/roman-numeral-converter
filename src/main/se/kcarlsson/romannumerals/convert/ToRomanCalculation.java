package se.kcarlsson.romannumerals.convert;

import static se.kcarlsson.romannumerals.convert.RomanSymbolUtils.toRomanSymbols;

class ToRomanCalculation {

    final String symbols;
    final int remainder;

    ToRomanCalculation(int n, int powerOf10, RomanSymbolRange range) {
        int divisor = (int) Math.pow(10, powerOf10);
        var quotient = n / divisor;
        if (powerOf10 >= 3)
            symbols = repeat(quotient, range.high);
        else
            symbols = toRomanSymbols(quotient, range);
        remainder = n % divisor;
    }

    private String repeat(int n, String symbol) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++)
            sb.append(symbol);
        return sb.toString();
    }
}
