package se.kcarlsson.romannumerals.convert;

class RomanSymbolUtils {

    final static RomanSymbolRange ivx = new RomanSymbolRange("I", "V", "X");
    final static RomanSymbolRange xlc = new RomanSymbolRange("X", "L", "C");
    final static RomanSymbolRange cdm = new RomanSymbolRange("C", "D", "M");

    static String toRomanSymbols(int n, RomanSymbolRange range) {
        if (n == 1) return range.low;
        if (n == 2) return range.low + range.low;
        if (n == 3) return range.low + range.low + range.low;
        if (n == 4) return range.low + range.mid;
        if (n == 5) return range.mid;
        if (n == 6) return range.mid + range.low;
        if (n == 7) return range.mid + range.low + range.low;
        if (n == 8) return range.mid + range.low + range.low + range.low;
        if (n == 9) return range.low + range.high;
        if (n == 10) return range.high;
        return "";
    }

    static int fromRomanSymbols(String s, RomanSymbolRange range) {
        if (s.equals(range.low)) return 1;
        if (s.equals(range.low + range.low)) return 2;
        if (s.equals(range.low + range.low + range.low)) return 3;
        if (s.equals(range.low + range.mid)) return 4;
        if (s.equals(range.mid)) return 5;
        if (s.equals(range.mid + range.low)) return 6;
        if (s.equals(range.mid + range.low + range.low)) return 7;
        if (s.equals(range.mid + range.low + range.low + range.low)) return 8;
        if (s.equals(range.low + range.high)) return 9;
        if (s.equals(range.high)) return 10;
        return -1;
    }
}
