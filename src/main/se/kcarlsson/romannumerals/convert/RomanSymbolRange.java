package se.kcarlsson.romannumerals.convert;

class RomanSymbolRange {

    final String low;
    final String mid;
    final String high;

    RomanSymbolRange(String low, String mid, String high) {
        this.low = low;
        this.mid = mid;
        this.high = high;
    }
}
