package se.kcarlsson.romannumerals.convert;

import static se.kcarlsson.romannumerals.convert.RomanSymbolUtils.fromRomanSymbols;

class FromRomanCalculation {

    final int number;
    final String remainder;

    private int i;

    FromRomanCalculation(String s, int powerOf10, RomanSymbolRange range) {
        if (powerOf10 >= 3)
            number = leastDominantNumberWithoutRomanTallies(s, range);
        else
            number = leastDominantNumber(s, powerOf10, range);
        this.remainder = s.substring(0, i + 1);
    }

    private int leastDominantNumberWithoutRomanTallies(String s, RomanSymbolRange range) {
        int n = 0;
        for (i = s.length() - 1; i >= 0; i--) {
            var nextSymbol = s.substring(i, i + 1);
            if (!nextSymbol.equals(range.high)) return n;
            n = n + 1000;
        }
        return n;
    }

    private int leastDominantNumber(String s, int powerOf10, RomanSymbolRange range) {
        int multiplier = (int) Math.pow(10, powerOf10);
        int n = 0;
        for (i = s.length() - 1; i >= 0; i--) {
            var nextSymbols = s.substring(i);
            int nextNumber = fromRomanSymbols(nextSymbols, range);
            if (nextNumber == -1) return n;
            n = multiplier * nextNumber;
        }
        return n;
    }
}