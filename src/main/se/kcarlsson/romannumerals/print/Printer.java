package se.kcarlsson.romannumerals.print;

public interface Printer {
    void print(String s);
    void print(int i);
    void printHelp();
}
