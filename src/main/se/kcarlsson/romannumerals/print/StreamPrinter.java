package se.kcarlsson.romannumerals.print;

import java.io.PrintStream;

public class StreamPrinter implements Printer {

    public static final String HELP_TEXT =
            "Usage: Main [options...] <arg>" +
            "\n" +
            "\n" +
            "  -tr, --to-roman     Convert a number to a roman numeral" +
            "\n" +
            "  -lc, --lower-case   Used with -tr to convert to lower case" +
            "\n" +
            "  -tn, --to-number    Convert a roman numeral to a number" +
            "\n";

    private final PrintStream printStream;

    public StreamPrinter(PrintStream printStream) {
        this.printStream = printStream;
    }

    @Override
    public void print(String s) {
        printStream.println(s);
    }

    @Override
    public void print(int i) {
        printStream.println(i);
    }

    @Override
    public void printHelp() {
        printStream.println(HELP_TEXT);
    }
}
