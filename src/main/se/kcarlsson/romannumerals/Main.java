package se.kcarlsson.romannumerals;

import se.kcarlsson.romannumerals.args.ArgumentParser;
import se.kcarlsson.romannumerals.convert.Converter;
import se.kcarlsson.romannumerals.print.StreamPrinter;

public class Main {

    public static void main(String[] args) {
        new ConversionJob(
                new Converter(),
                new ArgumentParser(),
                new StreamPrinter(System.out),
                args
        ).run();
    }
}
