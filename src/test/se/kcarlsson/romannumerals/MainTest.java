package se.kcarlsson.romannumerals;

import org.junit.jupiter.api.Test;

class MainTest {

    @Test
    void create_main() {
        new Main();
    }

    @Test
    void main_can_run() {
        Main.main(new String[]{});
        Main.main(new String[]{"-tn", "V"});
    }
}