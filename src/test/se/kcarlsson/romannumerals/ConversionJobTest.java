package se.kcarlsson.romannumerals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import se.kcarlsson.romannumerals.args.ArgumentParser;
import se.kcarlsson.romannumerals.convert.Converter;
import se.kcarlsson.romannumerals.print.PrintSpy;
import se.kcarlsson.romannumerals.print.StreamPrinter;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ConversionJobTest {

    private Converter converter;
    private ArgumentParser argParser;
    private PrintSpy printSpy;
    private StreamPrinter printer;

    @BeforeEach
    void setup() {
        converter = new Converter();
        argParser = new ArgumentParser();
        printSpy = new PrintSpy();
        printer = new StreamPrinter(printSpy);
    }

    @Test
    void create_conversion_job() {
        new ConversionJob(converter, argParser, printer, new String[]{"-tr", "100"});

        assertEquals(0, printSpy.noOfPrintedString());
        assertEquals(0, printSpy.noOfPrintedInts());
    }

    @Test
    void args_in_invalid_format_does_print_help() {
        var conversion = newConversion(new String[]{"-", "0"});

        conversion.run();

        assertEquals(1, printSpy.noOfPrintedString());
        assertEquals(StreamPrinter.HELP_TEXT, printSpy.printedString(0));
    }

    @Test
    void convert_to_number() {
        var conversion = newConversion(new String[]{"-tn", "IV"});

        conversion.run();

        assertEquals(1, printSpy.noOfPrintedInts());
        assertEquals(4, printSpy.printedInt(0));
    }

    @Test
    void convert_to_number_with_invalid_input_does_print_help() {
        var conversion = newConversion(new String[]{"-tn", "IB"});

        conversion.run();

        assertEquals(1, printSpy.noOfPrintedString());
        assertEquals(StreamPrinter.HELP_TEXT, printSpy.printedString(0));
    }

    @Test
    void convert_to_roman() {
        var conversion = newConversion(new String[]{"-tr", "14"});

        conversion.run();

        assertEquals(1, printSpy.noOfPrintedString());
        assertEquals("XIV", printSpy.printedString(0));
    }

    @Test
    void convert_to_lower_case_roman() {
        var conversion = newConversion(new String[]{"-tr", "-lc", "14"});

        conversion.run();

        assertEquals(1, printSpy.noOfPrintedString());
        assertEquals("xiv", printSpy.printedString(0));
    }

    @Test
    void convert_to_roman_with_invalid_input_does_print_help() {
        var conversion = newConversion(new String[]{"-tr", "0"});

        conversion.run();

        assertEquals(1, printSpy.noOfPrintedString());
        assertEquals(StreamPrinter.HELP_TEXT, printSpy.printedString(0));
    }

    private ConversionJob newConversion(String[] args) {
        return new ConversionJob(converter, argParser, printer, args);
    }
}
