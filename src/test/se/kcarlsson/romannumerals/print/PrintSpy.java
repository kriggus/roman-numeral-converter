package se.kcarlsson.romannumerals.print;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class PrintSpy extends PrintStream {

    private List<String> printlnStrings = new ArrayList<>();
    private List<Integer> printlnInts = new ArrayList<>();

    public PrintSpy() {
        super(new ByteArrayOutputStream());
    }

    @Override
    public void println(String x) {
        printlnStrings.add(x);
        super.println(x);
    }

    @Override
    public void println(int x) {
        printlnInts.add(x);
        super.println(x);
    }

    public String printedString(int ordering) {
        return printlnStrings.get(ordering);
    }

    public int noOfPrintedString() {
        return printlnStrings.size();
    }

    public int printedInt(int ordering) {
        return printlnInts.get(ordering);
    }

    public int noOfPrintedInts() {
        return printlnInts.size();
    }
}
