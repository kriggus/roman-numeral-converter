package se.kcarlsson.romannumerals.print;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StreamPrinterTest {

    @Test
    void create_writer() {
        new StreamPrinter(new PrintSpy());
    }

    @Test
    void print_does_print_to_println() {
        var spy = new PrintSpy();
        var printer = new StreamPrinter(spy);

        printer.print("Hey");
        printer.print("Hey again");

        assertEquals(2, spy.noOfPrintedString());
        assertEquals("Hey", spy.printedString(0));
        assertEquals("Hey again", spy.printedString(1));
    }

    @Test
    void print_does_ints_to_println() {
        var spy = new PrintSpy();
        var printer = new StreamPrinter(spy);

        printer.print(2);
        printer.print(-1);

        assertEquals(2, spy.noOfPrintedInts());
        assertEquals(2, spy.printedInt(0));
        assertEquals(-1, spy.printedInt(1));
    }

    @Test
    void printHelp_does_print_help_to_println() {
        var spy = new PrintSpy();
        var printer = new StreamPrinter(spy);

        printer.printHelp();

        assertEquals(1, spy.noOfPrintedString());
        assertEquals(StreamPrinter.HELP_TEXT, spy.printedString(0));
    }
}
