package se.kcarlsson.romannumerals.args;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ArgumentParserTest {

    private static ArgumentParser parser;

    @BeforeAll
    static void setup() {
        parser = new ArgumentParser();
    }

    @Test
    void should_convert_to_roman() {
        assertFalse(parser.shouldConvertToRoman(null));
        assertFalse(parser.shouldConvertToRoman(new String[]{}));
        assertFalse(parser.shouldConvertToRoman(new String[]{"-tr"}));
        assertFalse(parser.shouldConvertToRoman(new String[]{"--to-roman"}));
        assertFalse(parser.shouldConvertToRoman(new String[]{"tr"}));
        assertFalse(parser.shouldConvertToRoman(new String[]{"-to-roman"}));
        assertTrue(parser.shouldConvertToRoman(new String[]{"-tr", "12"}));
        assertTrue(parser.shouldConvertToRoman(new String[]{"-TR", "12"}));
        assertTrue(parser.shouldConvertToRoman(new String[]{"--to-roman", "14"}));
        assertTrue(parser.shouldConvertToRoman(new String[]{"--TO-ROMAN", "14"}));
    }

    @Test
    void should_convert_from_roman() {
        assertFalse(parser.shouldConvertFromRoman(null));
        assertFalse(parser.shouldConvertFromRoman(new String[]{}));
        assertFalse(parser.shouldConvertFromRoman(new String[]{"-tn"}));
        assertFalse(parser.shouldConvertFromRoman(new String[]{"--to-number"}));
        assertFalse(parser.shouldConvertFromRoman(new String[]{"tn", "VII"}));
        assertFalse(parser.shouldConvertFromRoman(new String[]{"-to-number", "VII"}));
        assertTrue(parser.shouldConvertFromRoman(new String[]{"-tn", "VII"}));
        assertTrue(parser.shouldConvertFromRoman(new String[]{"-TN", "VII"}));
        assertTrue(parser.shouldConvertFromRoman(new String[]{"--to-number", "VII"}));
        assertTrue(parser.shouldConvertFromRoman(new String[]{"--TO-NUMBER", "VII"}));
    }

    @Test
    void should_convert_to_lower_case() {
        assertFalse(parser.shouldConvertToLowerCase(null));
        assertFalse(parser.shouldConvertToLowerCase(new String[]{}));
        assertFalse(parser.shouldConvertToLowerCase(new String[]{"-tr", "1001"}));
        assertFalse(parser.shouldConvertToLowerCase(new String[]{"-tn", "-lc", "IV"}));
        assertTrue(parser.shouldConvertToLowerCase(new String[]{"-tr", "-lc"}));
        assertTrue(parser.shouldConvertToLowerCase(new String[]{"-tr", "-LC"}));
        assertTrue(parser.shouldConvertToLowerCase(new String[]{"-tr", "--lower-case"}));
        assertTrue(parser.shouldConvertToLowerCase(new String[]{"-tr", "--LOWER-CASE"}));
    }

    @Test
    void input_as_a_string() {
        assertEquals("", parser.inputAsString(null));
        assertEquals("", parser.inputAsString(new String[]{}));
        assertEquals("", parser.inputAsString(new String[]{"I"}));
        assertEquals("II", parser.inputAsString(new String[]{"junk", "II"}));
        assertEquals("IV", parser.inputAsString(new String[]{"-tn", "IV"}));
        assertEquals("IV", parser.inputAsString(new String[]{"-tr", "IV"}));
        assertEquals("IV", parser.inputAsString(new String[]{"-tr", "iv"}));
        assertEquals("1", parser.inputAsString(new String[]{"-tr", "1"}));
        assertEquals("II", parser.inputAsString(new String[]{"-tn", "II"}));
    }

    @Test
    void input_as_an_integer() {
        assertEquals(0, parser.inputAsInt(null));
        assertEquals(0, parser.inputAsInt(new String[]{}));
        assertEquals(0, parser.inputAsInt(new String[]{"1"}));
        assertEquals(1, parser.inputAsInt(new String[]{"junk", "1"}));
        assertEquals(1, parser.inputAsInt(new String[]{"-tr", "1"}));
        assertEquals(0, parser.inputAsInt(new String[]{"-tr", "I"}));
        assertEquals(0, parser.inputAsInt(new String[]{"-tn", "I"}));
        assertEquals(12344, parser.inputAsInt(new String[]{"-tr", "12344"}));
        assertEquals(4, parser.inputAsInt(new String[]{"-tr", "-lc", "4"}));
        assertEquals(12344, parser.inputAsInt(new String[]{"-tr", "--lower-case", "12344"}));
    }
}