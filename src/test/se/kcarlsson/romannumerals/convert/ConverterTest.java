package se.kcarlsson.romannumerals.convert;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ConverterTest {

    private static Converter converter;

    @BeforeAll
    static void setup() {
        converter = new Converter();
    }

    @Test
    void to_roman_numeral_with_invalid_input() {
        assertEquals("", converter.toRomanNumeral(-2));
        assertEquals("", converter.toRomanNumeral(0));
    }

    @Test
    void to_roman_numeral() {
        assertEquals("I", converter.toRomanNumeral(1));
        assertEquals("II", converter.toRomanNumeral(2));
        assertEquals("III", converter.toRomanNumeral(3));
        assertEquals("IV", converter.toRomanNumeral(4));
        assertEquals("V", converter.toRomanNumeral(5));
        assertEquals("VI", converter.toRomanNumeral(6));
        assertEquals("VII", converter.toRomanNumeral(7));
        assertEquals("VIII", converter.toRomanNumeral(8));
        assertEquals("IX", converter.toRomanNumeral(9));
        assertEquals("X", converter.toRomanNumeral(10));
        assertEquals("XIII", converter.toRomanNumeral(13));
        assertEquals("XIV", converter.toRomanNumeral(14));
        assertEquals("XVIII", converter.toRomanNumeral(18));
        assertEquals("XIX", converter.toRomanNumeral(19));
        assertEquals("XXXIX", converter.toRomanNumeral(39));
        assertEquals("XL", converter.toRomanNumeral(40));
        assertEquals("XLI", converter.toRomanNumeral(41));
        assertEquals("CIV", converter.toRomanNumeral(104));
        assertEquals("CVIII", converter.toRomanNumeral(108));
        assertEquals("CX", converter.toRomanNumeral(110));
        assertEquals("CXIII", converter.toRomanNumeral(113));
        assertEquals("DXII", converter.toRomanNumeral(512));
        assertEquals("DCCLXXXIX", converter.toRomanNumeral(789));
        assertEquals("CMXCIX", converter.toRomanNumeral(999));
        assertEquals("M", converter.toRomanNumeral(1000));
        assertEquals("MIX", converter.toRomanNumeral(1009));
        assertEquals("MXCIX", converter.toRomanNumeral(1099));
        assertEquals("MC", converter.toRomanNumeral(1100));
        assertEquals("MMXIX", converter.toRomanNumeral(2019));
        assertEquals("MMMCMXCIX", converter.toRomanNumeral(3999));
        assertEquals("MMMM", converter.toRomanNumeral(4000));
        assertEquals("MMMMMMMMMM", converter.toRomanNumeral(10000));
    }

    @Test
    void to_lower_case_roman_numeral_with_invalid_input() {
        assertEquals("", converter.toRomanNumeralLowerCase(-1));
        assertEquals("", converter.toRomanNumeralLowerCase(0));
    }

    @Test
    void to_lower_case_roman_numeral() {
        assertEquals("xvi", converter.toRomanNumeralLowerCase(16));
    }

    @Test
    void from_roman_numeral_with_invalid_input() {
        assertEquals(0, converter.fromRomanNumeral("undefined"));
        assertEquals(0, converter.fromRomanNumeral("MUMMMJ"));
        assertEquals(1000, converter.fromRomanNumeral("MUM"));
        assertEquals(10, converter.fromRomanNumeral("MUX"));
        assertEquals(0, converter.fromRomanNumeral("0"));
    }

    @Test
    void from_roman_numeral() {
        assertEquals(1, converter.fromRomanNumeral("I"));
        assertEquals(2, converter.fromRomanNumeral("II"));
        assertEquals(3, converter.fromRomanNumeral("III"));
        assertEquals(4, converter.fromRomanNumeral("IV"));
        assertEquals(5, converter.fromRomanNumeral("V"));
        assertEquals(6, converter.fromRomanNumeral("VI"));
        assertEquals(8, converter.fromRomanNumeral("VIII"));
        assertEquals(9, converter.fromRomanNumeral("IX"));
        assertEquals(10, converter.fromRomanNumeral("X"));
        assertEquals(12, converter.fromRomanNumeral("XII"));
        assertEquals(14, converter.fromRomanNumeral("XIV"));
        assertEquals(19, converter.fromRomanNumeral("XIX"));
        assertEquals(25, converter.fromRomanNumeral("XXV"));
        assertEquals(30, converter.fromRomanNumeral("XXX"));
        assertEquals(39, converter.fromRomanNumeral("XXXIX"));
        assertEquals(40, converter.fromRomanNumeral("XL"));
        assertEquals(41, converter.fromRomanNumeral("XLI"));
        assertEquals(104, converter.fromRomanNumeral("CIV"));
        assertEquals(789, converter.fromRomanNumeral("DCCLXXXIX"));
        assertEquals(999, converter.fromRomanNumeral("CMXCIX"));
        assertEquals(1000, converter.fromRomanNumeral("M"));
        assertEquals(1009, converter.fromRomanNumeral("MIX"));
        assertEquals(1099, converter.fromRomanNumeral("MXCIX"));
        assertEquals(1100, converter.fromRomanNumeral("MC"));
        assertEquals(2019, converter.fromRomanNumeral("MMXIX"));
        assertEquals(3999, converter.fromRomanNumeral("MMMCMXCIX"));
        assertEquals(4000, converter.fromRomanNumeral("MMMM"));
        assertEquals(9999, converter.fromRomanNumeral("MMMMMMMMMCMXCIX"));
    }

    @Test
    void to_and_from_roman_numeral() {
        for (int n = 1; n <= 10000; n++) {
            var roman = converter.toRomanNumeral(n);
            var number = converter.fromRomanNumeral(roman);
            assertEquals(n, number);
        }
    }
}