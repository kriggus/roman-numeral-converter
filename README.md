# Roman Numeral Converter

Console app to convert from and to roman numerals. Require **Java 12** and **JUnit5.3**. *Created for fun to train tdd :---)*
```
Usage: Main [options...] <arg>

  -tr, --to-roman     Convert a number to a roman numeral
  -lc, --lower-case   Used with -tr to convert to lower case
  -tn, --to-number    Convert a roman numeral to a number
```
